from setuptools import setup, find_packages
from codecs import open
from os import path

here = path.abspath(path.dirname(__file__))

setup(
	name='fbg_module',
	version='0.1.0.dev',
	author='Baptiste Mouillat',
    author_email='baptistemouillat@gmail.com',
	description='Interface for the FBG unit',
	url='https://bitbucket.org/sysengntnu/fbg-module',
    packages=['fbg_module'],
	)
