.. test documentation master file, created by
   sphinx-quickstart on Thu Jul 03 13:36:19 2014.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to fbg_module's documentation!
======================================

The fbd_module is an python interface for the fbg unit.

The the intended usage::
.. code-block:: python
    from fbg_module.fbg_module import FiberBraggGratingUnit
    fiber = FiberBraggGratingUnit(1)
Get all measurements from all sensors::
.. code-block:: python
    temperature, wavelength, strain = fiber.perform_measurement()
Get only temperature measurements from all sensors::
.. code-block:: python
    print fiber.perform_measurement_temperature()

Contents:
=========

.. toctree::
   :maxdepth: 2

.. automodule:: fbg_module
   :members:


Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

