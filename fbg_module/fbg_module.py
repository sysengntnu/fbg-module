from ctypes import *
import re
import os
import time
import ConfigParser
from os import path

"""Loading configuration"""
"""Test Test"""	

here = path.abspath(path.dirname(__file__))

FILE_PATH = {}
SENSOR_PATH = {}

filename = 'file_path.cfg'

parser = ConfigParser.SafeConfigParser()
parser.read(here + '/' + filename)

for i, j in parser.items('FILE_PATH'):
	FILE_PATH[i] = eval(j)
for i, j in parser.items('SENSOR_PATH'):
	SENSOR_PATH[i] = eval(j)


class FiberBraggGratingUnit():
	"""Test Test"""	

	def __init__(self, com_port=1, optical_input=1, averages=1):
		# Load configuration file


		# Lodad DLL file
		try :
			self.dll = WinDLL(FILE_PATH['dll_file'])
			print('dll version: ' + str(self.get_dll_version()))
		except :
			print('error loading dll file')
	
		self.com_port = com_port
		self.load_init_file()
		self.open_communication(self.com_port)

		if not self.test_data_communication(self.com_port):
			print('com error')

		self.setup_sensors()
		self.set_optical_input(optical_input)
		self.set_averages(averages)

		# CCD Autorange
		self.integration_time = 20.0
		self.autoranging()
		self.measure_dark_spectrum()

		print("initialization finished")
		print("FBG Unit ready")

	def __exit__(self, type, value, traceback):
		self.close_communication(self.com_port)

	def setup_sensors(self):
		keys = SENSOR_PATH.keys()
		for key in keys:
			array_number, sensor_number, path = SENSOR_PATH[key]
			self.setup_sensor(array_number, sensor_number, path)

	def set_optical_input(self, optical_input):
		if self.test_optical_switch():
			self.optical_input = optical_input
			print('optical input: ' + str(optical_input))
		else :
			print("No optical input wired")

		self.select_optical_input(self.optical_input)

	def set_averages(self, averages):
		self.averages = averages 
		print('Number of averages set on: ' + str(averages))



	def autoranging(self):
		"""
		*Loop for determining the correct integration time*
		"""
		for i in range(0,1000):
			self.set_integration_time(self.integration_time)
			self.measure(self.optical_input,0)
			self.get_max_intensity()
			if self.get_max_intensity() > 65000. :
				self.integration_time = self.integration_time/1.2
			else:
				print 'finished autoranging'
				break

	def measure_dark_spectrum(self):
		"""
		*Measure the dark spectrum for each input*
		"""
		self.measure_dark_current(0, self.integration_time)
		time.sleep(1)    # pause 1 seconds
		self.measure_dark_current(1, self.integration_time)
		self.measure_dark_current(2, self.integration_time)
		time.sleep(1)

	def perform_measurement_temperature(self):
		"""
		*Perform the measurement of wavelength, strain and temperature*
		"""
		self.measure(self.optical_input, self.averages)
		number_of_peaks = self.detect_peaks()
		self.calibrate()
		temperature = []

		for sensor_number in range(1, number_of_peaks + 1):
			temperature.append(self.get_temperature(sensor_number))
		return temperature

	def perform_measurement_wavelength(self):
		"""
		*Perform the measurement of wavelength, strain and temperature*
		"""
		self.measure(self.optical_input, self.averages)
		number_of_peaks = self.detect_peaks()
		self.calibrate()
		wavelength = []

		for sensor_number in range(1, number_of_peaks + 1):	
			wavelength.append(self.get_wavelength(sensor_number))
		return wavelength

	def perform_measurement_strain(self):
		"""
		*Perform the measurement of wavelength, strain and temperature*
		"""
		self.measure(self.optical_input, self.averages)
		number_of_peaks = self.detect_peaks()
		self.calibrate()
		strain =[]

		for sensor_number in range(1, number_of_peaks + 1):
			strain.append(self.get_strain(sensor_number))
		return strain


	def load_init_file(self):
		"""
		*Load the general initialization file*
		"""
		self.dll.Init_Unit.argtypes = [c_char_p]
		self.dll.Init_Unit(FILE_PATH['init_file'])

	def open_communication(self, com_number):
		"""
		*Open a communication between the instrument and the computer*

		:param com_number: number of the serial interface (1 for Com1 - 2 for Com2)
		:type com_number: int
		:rtype: int
		:returns:   - 0 = No error 
    				- 1509 = Error opening Com port 
    				- 1510 = Error setting Com port 
    				- 1514 = Error opening Com port for CCD spectrometer
    				- 1515 = Error setting CCD spectrometer baudrate 
    				- 1516 = Error loading CCD spectrometer settings 
    				- 1517 = Error loading CCD spectrometer calibration
    				- 1519 = Error loading CCD number of pixels

		"""
		self.dll.Open_Com.argtypes = [c_int]
		self.dll.Open_Com.restype = c_int
		return self.dll.Open_Com(com_number)

	def close_communication(self, com_number):
		"""
		*Close a communication between the instrument and the computer*

		:param com_number: number of the serial interface (1 for Com1 - 2 for Com2)
		:type com_number: int
		"""
		self.dll.Close_Com.argtypes = [c_int]
		self.dll.Close_Com(com_number)

	def test_data_communication(self, module_number):
		""" 
		*Perform a test of the communication port*

		:param module_number: the number of the module
		:type module_number: int
		:rtype: bool
		:returns: - True = Communication test successful
				  - False = Communication test failed
		"""
		self.dll.Test_Comm.argtypes = [c_int]
		self.dll.Test_Comm.restype = c_bool
		return self.dll.Test_Comm(module_number)

	def test_optical_switch(self):
		"""
		*Perform a test of the optical switch*

		:rtype: bool
		:returns: - True = test successful
				  - False = test failed
		"""
		self.dll.Test_Switch.restype = c_bool
		return self.dll.Test_Switch()

	def get_dll_version(self):
		"""
		*Return the version of the actual loaded DLL file*

		:rtype: double
		"""
		self.dll.Get_Version.restype = c_double
		return self.dll.Get_Version()

	def set_reference_wavelength(self, array_number, sensor_number, reference_wavelength):
		"""
		*Set the reference wavelength to calculate the normalized wavelength shift of the sensor*

		:param array_number: Number of the array (1 or 2)
		:type array_number: int
		:param sensor_number: Number of the sensor (1 to 9)
		:type sensor_number: int
		:param reference_wavelength: reference wavelength in number
		:type reference_wavelength: double
		"""
		self.dll.Set_Reference.argtypes = [c_int, c_int, c_double]
		self.dll.Set_Reference(array_number, sensor_number, reference_wavelength)

	def set_sensor_temperature(self, array_number, sensor_number, sensor_temperature):
		"""
		*Set the actual sensor temperature to calculate the shift of the sensor*

		:param array_number: Number of the array (1 or 2)
		:type array_number: int
		:param sensor_number: Number of the sensor (1 to 9)
		:type sensor_number: int
		:param sensor_temperature: reference wavelength in number
		:type sensor_temperature: double
		"""
		self.dll.Set_SensTemp.argtypes = [c_int, c_int, c_double]
		self.dll.Set_SensTemp(array_number, sensor_number, sensor_temperature)

	def set_reference_temperature(self, array_number, sensor_number, reference_temperature):
		"""
		*Set the reference temperature to calculate the sensor value*

		:param array_number: Number of the array (1 or 2)
		:type array_number: int
		:param sensor_number: Number of the sensor (1 to 9)
		:type sensor_number: int
		:param reference_temperature: reference wavelength in number
		:type reference_temperature: double
		"""
		self.dll.Set_RefTemp.argtypes = [c_int, c_int, c_double]
		self.dll.Set_RefTemp(array_number, sensor_number, reference_temperature)

	def calibrate(self):
		"""
		*Perform an internal calibration procedure*
		"""
		self.dll.Load_WL()

	def setup_sensor(self, array_number, sensor_number, filename):
		"""
		*Assign a sensor to the sensor number*

		:param array_number: Number of the array (1 or 2)
		:type array_number: int
		:param sensor_number: Number of the sensor (1 to 9)
		:type sensor_number: int
		:param filename: pointer to the name of the sensor initialization file
		:type filename: char_p 
		"""
		self.dll.Set_Sens.argtypes = [c_short, c_short, c_char_p]
		self.dll.Set_Sens(array_number, sensor_number, filename)

	def select_prestrain(self, array_number, sensor_number, prestrain):
		"""
		*Set the Pre-Strain to calculate the sensor value*

		:param array_number: Number of the array (1 or 2)
		:type array_number: int
		:param sensor_number: Number of the sensor (1 to 9)
		:type sensor_number: int
		:param prestrain: sensor strain offset value
		:type prestrain: double
		"""
		self.dll.Set_PreStrain.argtypes = [c_int, c_int, c_double]
		self.dll.Set_PreStrain(array_number, sensor_number, prestrain)

	def get_wavelength(self, sensor_number):
		"""
		*Calculate the sensor wavelength*

		:param sensor_number: Number of the sensor (1 to 9)
		:type sensor_number: int
		:rtype: double
		:returns: measured sensor wavelength in nm
		"""
		self.dll.get_WL.argtypes = [c_int]
		self.dll.get_WL.restype = c_double
		return self.dll.get_WL(sensor_number)

	def get_strain(self, sensor_number):
		"""
		*Calculate the strain of a sensor*

		:param sensor_number: Number of the sensor (1 to 9)
		:type sensor_number: int
		:rtype: double
		:returns: strain value
		:raises: - 502  GF=0 setting automatically to 0.79
				 - 503  reference wavelength=0 setting automatically to 1550nm
				 - 504  TK=0 setting automatically to 7.1	
		"""
		self.dll.get_Strain.argtypes = [c_int]
		self.dll.get_Strain.restype = c_double
		return self.dll.get_Strain(sensor_number)

	def get_temperature(self, sensor_number):
		"""
		*Calculate the temperature of the sensor*

		:param sensor_number: Number of the sensor (1 to 9)
		:type sensor_number: int
		:rtype: double
		:returns: temperature value
		:raises: - 503  reference wavelength=0 setting automatically to 1550nm
				 - 504  TK=0 setting automatically to 7.1
		"""
		self.dll.get_FBGTemp.argtypes = [c_int]
		self.dll.get_FBGTemp.restype = c_double
		return self.dll.get_FBGTemp(sensor_number)

	def measure_dark_current(self, mode, integration_time):
		"""
		*Measure the dark current for getting the dark spectrum for each input*

		:param mode: - 0 = switching the light source off
					 - 1 = measurement of dark currents aith light source off
					 - 2 = switching the light source on
		:type mode: int
		:param integration_time: Integration time
		:type integration_time: double
		"""
		self.dll.Meas_Dark.argtypes = [c_int, c_double]
		self.dll.Meas_Dark(mode, integration_time)

	def measure(self, input_channel, averages):
		"""
		*Start the measurments procedure*

		:param input_channel: Number of the input
		:type input_channel: int
		:param averages: Number of averages
		:type averages: int
		:rtype: bool
		:returns: True if OK
		"""
		self.dll.Measure.argtypes = [c_int, c_int]
		self.dll.Measure.restype = c_bool
		return self.dll.Measure(input_channel, averages)

	def select_optical_input(self, channel):
		"""
		*Operate the fiber-optic switch*

		:param channel: Number of the output channel
		:type channel: int
		"""
		self.dll.Switch_Out.argtypes = [c_int]
		self.dll.Switch_Out(channel)

	def set_DAC(self, voltage):
		"""
		*Control the Digital to Analog Converter*

		:param voltage: Output voltage 0 to 10V
		:type voltage: double
		:rtype: bool
		"""
		self.dll.DAC_Out.argtypes = [c_double]
		self.dll.DAC_Out.restype = c_bool
		return self.dll.DAC_Out(voltage)

	def get_ADC(self):
		"""
		*Read the voltage applied to the Analog to Digital Converter*

		:rtype: double
		:returns: Input voltage 0 to 10V
		"""
		self.dll.ADC_In.restype = c_double
		return self.dll.ADC_In()

	def get_spectrum(self, pixel_number, component):
		"""
		*Obtain a spectral distribution of the reflected light*

		:param pixel_number: Number of the pixel (0 to total_number_of_pixel-1)
		:type pixel_number: int
		:param component: - 1 = Normalized spectrum
						  - 2 = Raw spectrum
						  - 3 = Dark spectrum (measured spectrum when LED was off)
						  - 4 = LED spectrum from file SerNo.Led 
						  - 5 = Back reflected light induced by poor sensor termination
						  - 6 = Wavelength of the pixel
		:type component: int
		:rtype: double
		:returns: detected amplitude for the pixel pixel_number
		"""
		self.dll.Get_Spectrum.argtypes = [c_int, c_int]
		self.dll.Get_Spectrum.restype = c_double
		return self.dll.Get_Spectrum(pixel_number, component)

	def get_max_intensity(self):
		"""
		*Give the maximum amplitude of the measured spectrum*

		:rtype: double
		:returns: value of Max_Raw
		"""
		self.dll.Get_MaxRaw.restype = c_double
		return self.dll.Get_MaxRaw()

	def detect_peaks(self):
		"""
		Calculate the peaks wavelength from the acquired spectrum and return the number of detected FBG sensors*

		:rtype: int
		:returns: Detected number of FBG sensors
		"""
		self.dll.Calc_Peaks.restype = c_int
		return self.dll.Calc_Peaks()

	def set_integration_time(self, int_time):
		"""
		*Set the integration time*

		:param int_time: integration time of the spectrometer in msec 
		:type int_time: double
		:rtype: int
		:returns: 0 if OK - !=0 if integration time out of range
		"""
		self.dll.Set_IntTime.argtypes =[c_double]
		self.dll.Set_IntTime.restype = c_int
		return self.dll.Set_IntTime(int_time)

	def load_pixel_wavelength(self):
		"""
		*Load the wavelength calibration of the spectrometer*

		:raises: calibration files not found
		"""
		self.dll.Load_WL()

if __name__ == '__main__':
	fiber = FiberBraggGratingUnit(1)
	#temperature, wavelength, strain = fiber.perform_measurement()

	print fiber.perform_measurement_temperature()
